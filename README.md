An animated QML splashscreen featuring Hatsune Miku for fans of anime-styled artwork. The image has a random chance to be either a Hatsune Miku render (origins unknown), or Snow Miku 2018 (since it seemed apt for theme). Every part of the splashscreen is scaled based off of the screen resolution, but it is best viewed in a landscape setting.

Depends on QT5 Particle Effects

Original KSplash:
https://store.kde.org/p/1000465/

Hatsune Miku Render by D-Chana.
http://x-newmoon-x.deviantart.com/art/Hatsune-Miku-Render-Vocaloid-292006264

Snow Miku 2018 Render by Yasumo01
http://twitter.com/yasumo01
