#!/bin/bash
# Sekrit KDE Package Installer Script

packageID="splashscreen.SnowyNightMiku"
packageType="Plasma/LookAndFeel"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function install_package() {
  if [[ ! -z $(plasmapkg2 -l -t "$packageType" | grep "$packageID") ]]; then
    echo "- Updating local $packageName $packageType -"
    plasmapkg2 -t "$packageType" -u "$DIR/package"
  else
    echo "- Installing local $packageName $packageType -"
    plasmapkg2 -t "$packageType" -i "$DIR/package"
  fi
}
function remove_package() {
  if [[ ! -z $(plasmapkg2 -l -t $packageType | grep $packageID) ]]; then
    echo "- Removing local $packageName $packageType -"
    plasmapkg2 -t "$packageType" -r "$packageID"
  fi
}
## Restart plasmashell ##
function reset_plasmashell() {
  kquitapp5 plasmashell
  kstart5 plasmashell > /dev/null 2>&1
}

if [[ -z "$@" ]]; then
  install_package
  reset_plasmashell
  exit
fi

while (( "$#" )); do
  case "$1" in
    -r|--remove)
      remove_package
      shift 1
      ;;
    --) # end argument parsing
      shift
      break
      ;;
    -*|--*=) # unsupported flags
      echo "Error: Unsupported flag $1" >&2
      exit 1
      ;;
    *) # preserve positional arguments
      PARAMS="$PARAMS $1"
      shift
      ;;
  esac
done

# set positional arguments in their proper place
eval set -- "$PARAMS"

